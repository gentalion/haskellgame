module Game where

import Battle
import Menu
import Squad
import Data.Default
import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game
import InteractBattle
import Animate
import DrawBattle
import System.Exit

data GameState = InMenu | InBattle

data Game = Game { gamestate :: GameState
                 , menu :: Menu Game
                 , squads :: [Squad]
                 , battle :: Battle
                 , images :: [Picture]
                 }

instance Default Game where
    def = Game {gamestate = InMenu, menu = initMenu, squads = [], battle = def, images = []} 

initMenu :: Menu Game
initMenu = Menu (Button {fromX = -213, toX = 213, fromY = 200, toY = 300, buttonText = "Start game", buttonEffect = (\g -> g {gamestate = InBattle})}
                :[])

drawGame :: Game -> IO Picture
drawGame g =
    case (gamestate g) of
        (InMenu  ) -> return $ drawMenu $ menu g
        (InBattle) -> drawBattle $ battle g

handleInput :: Event -> Game -> IO Game
handleInput event g =
    case (gamestate g) of
        (InMenu  ) -> handleMenuInput event g
        (InBattle) -> do newBattle <- handleBattleInput event $ battle g
                         return $ g {battle = newBattle}

handleMenuInput :: Event -> Game -> IO Game
handleMenuInput event g = 
    case (event) of
        (EventKey ( SpecialKey     KeyEsc) Down _ pos) -> exitSuccess
        (EventKey (MouseButton LeftButton) Down _ pos) -> return $ (buttonPress pos $ menu g) $ g
        (                                           _) -> return g

animateGame :: Float -> Game -> IO Game
animateGame f g =
    case (gamestate g) of
        (InMenu  ) -> return g
        (InBattle) -> do newBattle <- animateBattle f $ battle g
                         case (checkGameState $ newBattle) of 
                            (Playing) -> return $ g {battle = newBattle}
                            (    Win) -> return $ g {gamestate = InMenu}
                            (   Lose) -> return $ g {gamestate = InMenu}
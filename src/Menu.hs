{-# LANGUAGE RecordWildCards #-}

module Menu where

import Graphics.Gloss
import Const
import Data.Default

data Button a = Button { fromX :: Float
                       , toX :: Float
                       , fromY :: Float
                       , toY :: Float
                       , buttonText :: String
                       , buttonEffect :: (a -> a)
                       }

instance Default (Button a) where
    def = Button {fromX = 0, toX = 0, fromY = 0, toY = 0, buttonText = "", buttonEffect = id} 

data Menu a = Menu [Button a]

translate' :: Point -> Picture -> Picture
translate' pos pic = translate (fst pos) (snd pos) pic


drawButton :: Button a -> Picture
drawButton Button{..} = 
    pictures $ ((translate (fromX + (toX - fromX) / 2) (fromY + (toY - fromY) / 2)  $ rectangleSolid (toX - fromX) (toY - fromY))
               :(color white $ translate (fromX + 20.0) (fromY - 10.0 + (toY - fromY) / 2) $ scale 0.25 0.25 $ text buttonText)
               :[])

drawMenu :: Menu a -> Picture
drawMenu (Menu buttonList) = pictures ((translate (-77.0) 350.0 $ scale 0.5 0.5 $ text "Menu")
                                      :(map drawButton buttonList))

buttonPress :: (Float, Float) -> Menu a -> (a -> a)
buttonPress (x,y) (Menu buttonList) = 
    case (filter (\b -> x > fromX b && x < toX b && y > fromY b && y < toY b) buttonList) of
        ([]) -> id
        ( l) -> buttonEffect $ head l
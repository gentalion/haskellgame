module Animate where

import Battle
import MovingSquad (MovingSquad)
import qualified MovingSquad as MS
import Squad
import Combat
import EnemyAI
import Data.Default

animateBattle :: Float -> Battle -> IO Battle
animateBattle f b = case (animation b, movingEnemies b, turn b) of
    ([],[],Enemy ) -> return $ initPlayerTurn b
    ([],[],Player) -> return $ b
    ([], l,Enemy ) -> return $ enemyAIturn b
    ([ms],_,    _) -> 
        case (MS.animation ms) of
            ([]) -> attackAnybody (cellFromMovingSquad b ms) $ b
            ( _) -> return $ b {animation = [MS.animateMovingSquad f ms]}
    (anim, _,   _) ->  
        case (foldr1 (++) $ map MS.animation anim) of
            ([]) -> return 
                    $ checkEnemyTargetsOnField 
                    $ foldr modifyBattleWithCell (b {animation = []}) 
                    $ filter (\cell -> (floor $ squadPower $ maybe def id $ squad cell) > 0)
                    $ map (cellFromMovingSquad b) anim
            ( _) -> return $ b {animation = checkAllWaiting $ map (MS.animateMovingSquad f) anim}

initPlayerTurn :: Battle -> Battle
initPlayerTurn b = b { turn = Player
                     , allies = map (\x -> x {squad = (\s -> Just $ s {steps = maxSteps s}) $ maybe def id $ squad x}) $ allies b
                     }

cellFromMovingSquad :: Battle -> MovingSquad -> Cell
cellFromMovingSquad b ms = (getCell b (MS.destination ms)) {squad = Just $ squad {rotation = MS.rotation ms}}
    where squad = MS.squad ms

checkAllWaiting :: [MovingSquad] -> [MovingSquad]
checkAllWaiting msList = if any null animationList then msList else
    let waitingList = map MS.waiting $ map (either id def) $ map head $ animationList
    in case (all (\x -> x == False) waitingList) of
        (True ) -> map (\ms -> ms {MS.animation = tail $ MS.animation ms}) msList
        (False) -> msList
    where animationList = map MS.animation $ msList 
module EnemyAI where

import Battle
import Distances
import qualified MovingSquad as MS
import Squad
import Const
import Hex (Position, evenrToPixel)
import Graphics.Gloss (Point)
import Data.Default

singleEnemyTurnToKnownTarget :: Battle -> Cell -> (Int,Cell) -> Battle
singleEnemyTurnToKnownTarget b c1 (dist,c2) =
    let map = buildMarchDistanceMap dist b c2
        c1squad = maybe def id $ squad c1
        rot = rotation c1squad
        --movementAnimation = buildRouteReversed b size c1 rot c2
        movementAnimation = nextRouteStep size dist (maxSteps c1squad) (position c1) rot map
        size = hexSize b
    in case (dist) of
        (9999) -> b
        (   _) -> b { otherCells = (c1 {squad = Nothing}):(otherCells b)
                    , enemies = excludeCell (enemies b) c1
                    , animation = (def { MS.squad = c1squad
                                       , MS.position = evenrToPixel size $ position c1
                                       , MS.rotation = rot
                                       , MS.animation = movementAnimation
                                       , MS.destination = MS.destinationFromAnimation movementAnimation size
                                       }):[]
                    }

singleEnemyTurn' :: Battle -> Cell -> Battle
singleEnemyTurn' b c = singleEnemyTurnToKnownTarget b c 
                              $ foldr (\x y -> if fst x > fst y then y else x) (9999,def) $ map (\x -> (getMarchDistance b c x, x))
                              $ map (getCell b) (targetsForEnemies b)


singleEnemyTurnSimple :: Battle -> Cell -> Battle
singleEnemyTurnSimple b c1 =
    let c1squad = maybe def id $ squad c1
        rot = rotation c1squad
        size = hexSize b
        neighbors = filter notObstacle $ getNeighbors b c1
        targetsForEnemiesCells = map (getCell b) $ targetsForEnemies b
        straightDistanceCur = map (getStraightDistance c1) $ targetsForEnemiesCells
    in case (filter (\x -> any (\t -> t == True) $ zipWith (>) straightDistanceCur $ map (getStraightDistance x) $ targetsForEnemiesCells) neighbors) of
        ([]) -> b
        ( l) ->
            b { otherCells = (c1 {squad = Nothing}):(otherCells b)
              , enemies = excludeCell (enemies b) c1
              , animation = (def { MS.squad = c1squad
                                       , MS.position = evenrToPixel size $ position c1
                                       , MS.rotation = rot
                                       , MS.animation = nextRouteStep size 1 1 (position c1) rot [(0, position $ head l)]
                                       , MS.destination = position $ head l
                                       }):[]
              } 

-- turn for enemyAI
enemyAIturn :: Battle -> Battle
enemyAIturn b =
    let movEnemies = movingEnemies b
    in (singleEnemyTurnSimple b (head movEnemies)) {movingEnemies = tail movEnemies}

checkEnemyTargets :: Battle -> Cell -> Bool
checkEnemyTargets b c = any (\x -> x == (position c)) (targetsForEnemies b)

checkEnemyTargetsOnField :: Battle -> Battle
checkEnemyTargetsOnField b =
    let hasCome = filter (\x -> maybe False (\x -> True) $ squad x) $ map (getCell b) (targetsForEnemies b)
    in foldr (\x res -> if Enemy == (maybe NoControl control $ squad x) then modifyBattleWithCell (x {squad = Nothing}) res else res) b hasCome
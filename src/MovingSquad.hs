{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DuplicateRecordFields #-}

module MovingSquad where

import Squad (Squad)
import qualified Squad as Squad
import Hex (Position)
import Graphics.Gloss (Point)
import Data.Default
import Data.Fixed
import Const

data Rotation = Rotation { deltaR :: Float
                         , tmaxR :: Float
                         , waiting :: Bool
                         }
instance Default Rotation where
    def = Rotation {deltaR = 0.0, tmaxR = 0.0, waiting = True} 

data Movement = Movement { deltaM :: Point
                         , tmaxM :: Point
                         }

type MSAnimation = Either Rotation Movement

data MovingSquad = MovingSquad { squad :: Squad
                               , position :: Point
                               , rotation :: Float
                               , animation :: [MSAnimation]
                               , destination :: Position
                               }

instance Default MovingSquad where
  def = MovingSquad {squad = def, position = (-2.0,-2.0), rotation = 0.0, animation = [], destination = (-2,-2)} 

animateMovingSquad :: Float -> MovingSquad -> MovingSquad
animateMovingSquad f ms = case (animation ms, head $ animation ms) of
    ([],             _) -> ms
    ( _,Left  rotation) -> animateRotation f rotation (ms {animation = tail $ animation ms})
    ( _,Right movement) -> animateMovement f movement (ms {animation = tail $ animation ms})

angleComparison :: Float -> Float -> Float -> Bool
angleComparison x1 dx x2 = case (x2 > x1, dx > 0) of
    ( True,  True) -> x1 + dx >= x2
    (False, False) -> x1 + dx <= x2
    ( True, False) -> x1 + dx <= x2 - 360.0
    (False,  True) -> x1 + dx >= x2 + 360.0

--animateRotation :: Float -> Rotation -> MovingSquad -> MovingSquad
--animateRotation f rot ms = ms {animation = (Left rot):(animation ms)}

animateRotation :: Float -> Rotation -> MovingSquad -> MovingSquad
animateRotation f rot ms | (rotation ms == tmaxR rot) && (waiting rot == True) = ms
                         | (rotation ms == tmaxR rot) && (waiting rot == False) = ms {animation = (Left rot):(animation ms)} 
                         | (angleComparison (rotation ms) (f * deltaR rot) (tmaxR rot)) 
                                = ms {rotation = tmaxR rot, animation = (Left $ rot {waiting = not $ waiting rot}):(animation ms)}
                         | otherwise = ms { rotation = (rotation ms) + (f * deltaR rot), animation = (Left rot):(animation ms)}


(+++) :: Point -> Point -> Point
(+++) (x1, y1) (x2, y2) = (x1 + x2, y1 + y2)

pointComparison :: Point -> Point -> Point -> Bool
pointComparison (x1,y1) (dx,dy) (x2,y2) = case (dx > 0, dy > 0) of
    ( True,  True) -> x1+dx >= x2 && y1+dy >= y2
    (False, False) -> x1+dx <= x2 && y1+dy <= y2
    ( True, False) -> x1+dx >= x2 && y1+dy <= y2
    (False,  True) -> x1+dx <= x2 && y1+dy >= y2

deltaPoint :: Point -> Point -> Float -> Point
deltaPoint (x1, y1) (x2, y2) divider = ((x2 - x1) / divider, (y2 - y1) / divider)

multiplyFloatPoint :: Float -> Point -> Point
multiplyFloatPoint f (px,py) = (f * px, f * py)

--animateMovement :: Float -> Movement -> MovingSquad -> MovingSquad
--animateMovement f mov ms = ms {animation = (Right mov):(animation ms)}

animateMovement :: Float -> Movement -> MovingSquad -> MovingSquad
animateMovement f mov@Movement{..} ms | pointComparison (position ms) (multiplyFloatPoint f deltaM) tmaxM = ms {position = tmaxM}
                                      | otherwise = ms {position = ((position ms) +++ (multiplyFloatPoint f deltaM)), animation = (Right mov):(animation ms)}

minimalRotation :: Bool -> Float -> Float -> Rotation
minimalRotation wait angle1 angle2 = 
    case (abs (angle2 - angle1) <= 180.0) of
        ( True) -> Rotation { tmaxR = angle2
                            , deltaR = signum (angle2 - angle1) * 60.0 / rotationOn60DegreesAnimationTime * 360.0 / fromIntegral framesPerSecond
                            , waiting = wait}
        (False) -> Rotation { tmaxR = angle2
                            , deltaR = signum (180.0 - (angle2 - angle1)) * 60.0 / rotationOn60DegreesAnimationTime * 360.0 / fromIntegral framesPerSecond
                            , waiting = wait}

generateMovement :: Bool -> Point -> Point -> Float -> Float -> [MSAnimation]
generateMovement wait pos destinationPos rot destinationRot | (rot /= destinationRot) =
                                                                  ((Left $ minimalRotation wait rot destinationRot)
                                                                  :(Right $ Movement {tmaxM = destinationPos, deltaM = deltaPoint pos destinationPos $ movementAnimationTime})
                                                                  :[])
                                                            | (rot == destinationRot) && (wait == True) =
                                                                  ((Left $ minimalRotation False rot destinationRot)
                                                                  :(Right $ Movement {tmaxM = destinationPos, deltaM = deltaPoint pos destinationPos $ movementAnimationTime})
                                                                  :[])
                                                            | otherwise =
                                                                  ((Right $ Movement {tmaxM = destinationPos, deltaM = deltaPoint pos destinationPos $ movementAnimationTime})
                                                                  :[])

destinationFromAnimation :: [MSAnimation] -> Float -> Position
destinationFromAnimation animation size =
    let lastA = last animation
        sizeTuple = (size, windowWidth, windowHeight)
    in case (lastA) of
        (Left  _) -> (-2,-2)
        (Right m) -> pixelToEvenr sizeTuple $ naturalOffset sizeTuple $ tmaxM m

getSmth :: [MSAnimation] -> String
getSmth [] = ""
getSmth ((Left x):xs) = show $ tmaxR x
getSmth ((Right x):xs) = show $ tmaxM x

getSmth' :: MovingSquad -> [MSAnimation] -> String
getSmth' ms [] = ""
getSmth' ms ((Left x):xs) = show $ deltaR x
getSmth' ms ((Right x):xs) = show $ (position ms)+++(deltaM x)

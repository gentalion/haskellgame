module Combat where

import Battle
import Distances
import Squad
import System.Random
import Hex (Position,evenrToPixel)
import MovingSquad (MovingSquad)
import qualified MovingSquad as MS
import Data.Default
import Const
import Graphics.Gloss (Point)
import EnemyAI
import Test.QuickCheck (generate, elements)

attackAnybody :: Cell -> Battle -> IO Battle
attackAnybody c b = do
    whomToAttackNotIO <- whomToAttack b c
    case (checkEnemyTargets b c, whomToAttackNotIO) of
        (True,                    _) -> return $ checkEnemyTargetsOnField $ modifyBattleWithCell c $ b {animation = []}
        (_,Nothing                 ) -> return $ checkEnemyTargetsOnField $ modifyBattleWithCell c $ b {animation = []}
        (_,Just (cellToAttack, rot)) -> attackExactSquad (b {animation = []}) c rot cellToAttack

whomToAttack :: Battle -> Cell -> IO (Maybe (Cell,Float))
whomToAttack b c = 
    let control_ = control $ maybe def id $ squad c
        neighboringSquads = filter (\(control,cellrot) -> control /= NoControl && control /= control_)
                            $ map (\(squad,cellrot) -> (maybe NoControl control squad,cellrot))
                            $ filter (\(squad,cellrot) -> maybe False (\any -> True) squad) 
                            $ map (\(cell,rot) -> (squad cell, (cell, rot))) 
                            $ getNeighborsWithRotToThem b c
    in case (neighboringSquads) of
        ([]) -> return Nothing
        ( l) -> do buf <- generate.elements $ map snd l
                   return $ Just buf

movePartOfDistance :: Point -> Point -> Point
movePartOfDistance (f1,f2) (d1,d2) = (f1 + (d1 - f1) * movementCoefficientWhenAttacking, f2 + (d2 - f2) * movementCoefficientWhenAttacking)

attackExactSquad :: Battle -> Cell -> Float -> Cell -> IO Battle
attackExactSquad b c1 c1DestRot c2 = 
    let c2DestRot = oppositeDirection c1DestRot
        size = hexSize b
        c1Point = evenrToPixel size $ position c1
        c2Point = evenrToPixel size $ position c2
        c1Squad = (maybe def id $ squad c1) {steps = 0}
        c2Squad = (maybe def id $ squad c2) {steps = 0}
        c2emptyCell = c2 {squad = Nothing}
    in do (c1SquadAfter, c2SquadAfter) <- attackEachOther c1Squad c2Squad
          return $ checkEnemyTargetsOnField $ modifyBattleWithCell c2emptyCell $ b {animation = ((generateMovingSquadMovingThereAndBack c1SquadAfter c1Point (movePartOfDistance c1Point c2Point) c1DestRot $ position c1)
                                                                 :(generateMovingSquadMovingThereAndBack c2SquadAfter c2Point (movePartOfDistance c2Point c1Point) c2DestRot $ position c2)
                                                                 :[])}
attackEachOther :: Squad -> Squad -> IO (Squad, Squad)
attackEachOther c1 c2 = do
                        apk1 <- randomRIO (minApk, maxApk)
                        apk2 <- randomRIO (minApk, maxApk)
                        dpk1 <- randomRIO (minDpk, maxDpk)
                        dpk2 <- randomRIO (minDpk, maxDpk)
                        return $ (c1 {sMods = (attackFixed (squadPower c2) (squadPower c1) apk1 dpk1):(sMods c1)}
                                 ,c2 {sMods = (attackFixed (squadPower c1) (squadPower c2) apk2 dpk2):(sMods c2)})

generateMovingSquadMovingThereAndBack :: Squad -> Point -> Point -> Float -> Position -> MovingSquad
generateMovingSquadMovingThereAndBack squad start stop destRot destPos =
    MS.MovingSquad { MS.squad = squad
                   , MS.position = start
                   , MS.rotation = rot
                   , MS.animation = (MS.generateMovement True start stop rot destRot)++(MS.generateMovement False stop start destRot destRot)
                   , MS.destination = destPos
                   }
        where rot = rotation squad

{-synchronizeEndOfRotation :: [MovingSquad] -> [MovingSquad]
synchronizeEndOfRotation msList =
    let animationList = map MS.animation msList
        rotationList = map (either id def) $ map head animationList
        maxRotTime = maximum $ map MS.waiting rotationList
    in zipWith (\ms rot -> ms {MS.animation = (Left $ rot {MS.waiting = maxRotTime}):(tail $ MS.animation ms)}) msList rotationList-}